from flask import Flask, jsonify, request
from flask_restx import Api, Resource, fields
from flask_jwt_extended import JWTManager, jwt_required, create_access_token

from flask_wtf.csrf import CSRFProtect

app = Flask(__name__)
app.config['JWT_SECRET_KEY'] = 'jwt-key'
app.config['SECRET_KEY'] = 'secret-key'
jwt = JWTManager(app)
# csrf = CSRFProtect(app)

api = Api(
    app,
    version='1.0',
    title='Sample API',
    description='A simple API with Swagger UI and JWT Authentication',
    security='Bearer Auth', 
    authorizations={  
        'Bearer Auth': {
            'type': 'apiKey',
            'in': 'header',
            'name': 'Authorization'
        },
    }
)

@app.route('/login', methods=['POST'])
def login():
    username = request.json.get('username', None)
    password = request.json.get('password', None)
    if username != 'admin' or password != 'admin':
        return jsonify({"msg": "Bad username or password"}), 401

    access_token = create_access_token(identity=username)
    return jsonify(access_token=access_token)

fake_db = [
    {'id': 1, 'name': 'Item 1', 'description': 'Description of Item 1'},
    {'id': 2, 'name': 'Item 2', 'description': 'Description of Item 2'}
]

item_model = api.model('Item', {
    'id': fields.Integer(readonly=True, description='The item unique identifier'),
    'name': fields.String(required=True, description='The item name'),
    'description': fields.String(required=True, description='The item description'),
})

ns = api.namespace('items', description='Operations related to items')

@ns.route('/')
class ItemList(Resource):
    @api.doc(security='Bearer Auth')
    @ns.doc('list_items')
    @ns.marshal_list_with(item_model)
    @jwt_required()  
    # @csrf.exempt
    def get(self):
        '''List all items'''
        return fake_db

    @ns.doc('create_item')
    @ns.expect(item_model)
    @ns.marshal_with(item_model, code=201)
    @jwt_required()
    # @csrf.exempt
    def post(self):
        '''Create a new item'''
        new_item = api.payload
        new_item['id'] = max(item['id'] for item in fake_db) + 1
        fake_db.append(new_item)
        return new_item, 201

@ns.route('/<int:id>')
@ns.response(404, 'Item not found')
@ns.param('id', 'The item identifier')
class Item(Resource):
    @api.doc(security='Bearer Auth')
    @ns.doc('get_item')
    @ns.marshal_with(item_model)
    @jwt_required()  
    # @csrf.exempt
    def get(self, id):
        '''Fetch an item given its identifier'''
        item = next((item for item in fake_db if item['id'] == id), None)
        if item:
            return item
        api.abort(404, "Item not found.")

if __name__ == '__main__':
    app.run(debug=True)
